Docker Poetry
=============
[![pipeline status][pipeline]][homepage]

Docker Python Image overlay for Poetry package manager.

Usage
-----
```dockerfile
FROM registry.gitlab.com/amalchuk/docker-poetry:1.1.13-py3.10
```

Distribution
------------
This project is licensed under the terms of the [MIT License](LICENSE).

[homepage]: <https://gitlab.com/amalchuk/docker-poetry>
[pipeline]: <https://gitlab.com/amalchuk/docker-poetry/badges/master/pipeline.svg?style=flat-square>
